<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('/', function () {
//    return view('front_end.home');
//});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {


    //==========================Bankend  Route================================================//
    
    Route::any('/admin', array('as' => 'admin', 'uses' => 'Admin\AdminUser@loginForm'));
    Route::any('/adminLoginCheck', array('as' => 'adminLoginCheck', 'uses' => 'Admin\AdminUser@adminLoginCheck'));
    Route::any('/adminDashboard', array('as' => 'adminDashboard', 'uses' => 'Admin\AdminUser@adminDashboard'));
    Route::any('/doctorDashboard', array('as' => 'doctorDashboard', 'uses' => 'Admin\AdminUser@doctorDashboard'));
    Route::any('/patientDashboard', array('as' => 'patientDashboard', 'uses' => 'Admin\AdminUser@patientDashboard'));
    
    
    Route::get('/logout', array('as' => 'logout', 'uses' => 'Admin\AdminUser@getLogOut')); 

   
    //==========================Front End  Route================================================//
    
    Route::any('/', array('as' => 'homePage', 'uses' => 'FrontEnd\HomeController@homePage'));
    
    Route::any('/doctorRegDetail',array('as' => 'doctorRegDetail', 'uses' => 'FrontEnd\HomeController@doctorRegDetail')); 
    Route::any('/patientRegDetail',array('as' => 'patientRegDetail', 'uses' => 'FrontEnd\HomeController@patientRegDetail')); 
    Route::any('/joinPage',array('as' => 'joinPage', 'uses' => 'FrontEnd\HomeController@joinPage'));
    Route::any('/signUpDoctor',array('as'=>'signUpDoctor','uses'=>'FrontEnd\HomeController@signUpDoctor'));
    Route::any('/signUpPatient',array('as'=>'signUpPatient','uses'=>'FrontEnd\HomeController@signUpPatient'));
    Route::any('/signIn',array('as'=>'signIn','uses'=>'FrontEnd\HomeController@signIn'));
   
    Route::post('/addPatient',array('as'=>'addPatient','uses'=>'FrontEnd\HomeController@addPatient'));
    Route::post('/addDoctor', array('as' => 'addDoctor', 'uses' => 'FrontEnd\HomeController@addDoctor')); 
    Route::any('/registrationSuccessMassege', array('as' => 'registrationSuccessMassege', 'uses' => 'FrontEnd\HomeController@registrationSuccessMassege'));
    Route::any('/confiormEmail/{id}', array('as' => 'confiormEmail', 'uses' => 'FrontEnd\HomeController@confiormEmail')); 
    
     

});
