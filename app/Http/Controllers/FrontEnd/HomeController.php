<?php

namespace App\Http\Controllers\FrontEnd;

use Illuminate\Http\Request;
use View;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Redirect;
use App\User;
use Hash;
use Mail;

class HomeController extends Controller
{
    public function homePage(){
        $data = array(
            'title'=>'Home'
        );
        return View::make('front_end.home')->with($data);
    }
    
    public function addDoctor(){
          $validation_rule = array(
            'name'         => array('required', 'between:3,50'),
            'username'     => array('required', 'between:3,50'),
            'email'        => array('required','email','unique:users'),  
            'nidno'        => array('required'),
            'mobile'       => array('required','numeric'),
            'password'     => array('required','between:5,20'),
          );
          
          $validation = Validator::make(Input::all(), $validation_rule);
          if($validation -> fails()){
              return Redirect::to('/')
                            ->withInput()
                            ->withErrors($validation,'doctor');
          }else{
            //Insert data into db
             $doctorInfo = array(
                'name'          => Input::get('name'),
                'username'      => Input::get('username'),
                'email'         => Input::get('email'),
                'password'      => Hash :: make(Input::get('password')),
                'nid'           => Input::get('nidno'),
                'doctor_reg_id' => '',
                'mobile'        => Input::get('mobile'),
                'biography'     => Input::get('biography'),
                'role'          => Input::get('role'),
                'status'        => 0,
                'mailconfiorm'   => 0
             );
             
             $doctor_id = User::createDoctor($doctorInfo);
              //Send a mail to the Doctor
              $data = array(
                'id'  => $doctor_id,
                'name' => Input::get('doctor_name')
              );
              
              Mail::send('emails.email_ conformation', $data, function ($message) {
                $message->from('subrata.sarkar88@gmail.com', 'Telemedicine');
                $message->subject('Email Confiormation for Telemedicine profile ');
                $message->to(Input::get('email'));
            });
              // Redriect to a new page with Email chacking massege.
          return Redirect::to('/registrationSuccessMassege');
          }
    }
    
    public function registrationSuccessMassege(){
        return View::make('front_end.pages.reg_success_massege'); 
    }
    
    
    public function confiormEmail($id){
        User::confiormDoctorEmail($id);
        
         Mail::send('emails.emailconfiorm_to_admin', ['key'=>'value'], function ($message) {
                $message->from('subrata.sarkar88@gmail.com', 'Telemedicine');
                $message->subject('Email Confiormed ');
                $message->to('aich@tech-novelty.com');
            });
        return View::make('front_end.pages.mail_conform_success_massege'); 
        // Update doctor table and confiorm accordin to this id;
    }

    public function addPatient(){
        $validation_rule = array(
            'patient_name'         => array('required', 'between:6,12'),
            'patient_username'     => array('required', 'between:5,10'),
            'patient_nidno'        => array('required'),
            'patient_mobile'       => array('required','numeric'),
            'patient_email'        => array('required','email','unique:patients'),
            'patient_password'     => array('required','between:5,20'),
          );
          
          $validation = Validator::make(Input::all(), $validation_rule);
          if($validation -> fails()){
              return Redirect::to('/')
                            ->withInput()
                            ->withErrors($validation,'patient');
          }else{
              
               //Send a mail to the Doctor
              //Insert data into db
              // Redriect to a new page with Email chacking massege.
          }
    }
    
    
    
    // Doctors details page
    public function doctorRegDetail(){
        $data = array(
            'title'=>'Doctor registration Details'
        );
        return View::make('front_end.pages.more_details.about_doctor_registration')->with($data);
              
    }
    
    //Doctor singup form
    
    public function signUpDoctor(){
        $data = array(
            'title'=>'Doctor SignUp Form'
        );
        return View::make('front_end.pages.form.doctor_sign_up_form')->with($data);
    }


    // Patients details page
    public function patientRegDetail(){
        $data = array(
            'title'=>'Patient registration Details'
        );
        return View::make('front_end.pages.more_details.about_patient_registration')->with($data);
              
    }
    
    //Patient Sign upForm
    
    public function signUpPatient(){
        $data = array(
            'title'=>'Patient Sing Up Form'
        );
        return View::make('front_end.pages.form.patient_sign_up_form')->with($data);
    }


    // Join Page
    
    public function joinPage(){
         $data = array(
            'title'=>'Join Details'
        );
        return View::make('front_end.pages.more_details.join_page')->with($data);
    }
    
    public function signIn(){
        $data = array(
            'title'=>'Sing In'
        );
        return View::make('front_end.pages.form.sign_in_form')->with($data); 
    }



//
}
