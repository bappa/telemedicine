<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Validator;
use View;
use Hash;
use Redirect;


class AdminUser extends Controller {

    
    //Admin login Form
    public function loginForm() {
        $data = array(
            'title' => 'Login Page'
        );
        return view::make('admin.pages.login_form')->with($data);
    }

    
    
    // Admin Login Check
    public function adminLoginCheck() {

        $validation_rule = [
            'username' => array('required', 'min:3', 'max:50'),
            'password' => array('required', 'min:6', 'max:50')
        ];

        $validation = Validator::make(Input::all(), $validation_rule);

        if ($validation->fails()) {
            return redirect()->back()
                            ->withInput()
                            ->withErrors($validation);
        } else {
            $athentication = Auth::attempt(array('username' => Input :: get('username'), 'password' => Input :: get('password'), 'status' => 1,'mailconfiorm'=> 1 ));
            if ($athentication) {
                $rememberme = Input::get('remember');
                if (!empty($rememberme)) {
                    //Remember Login data
                    Auth::loginUsingId(Auth::user()->id, true);
                }
                $role = Auth::user()->role;
                switch ($role) {
                    case 'admin':
                        return Redirect::intended('adminDashboard');
                        break;
                    case 'doctor':
                        return Redirect::intended('doctorDashboard');
                        break;
                    case 'patient':
                        return Redirect::intended('patientDashboard');
                        break;
                    default :
                        return Redirect::intended('adminDashboard');
                    
                }
                
            }  else {//Athentication End
                return Redirect::to('/admin')->withInput()->with('authentication_error', 'Invalid username/Password!');
            }
        }
    }
    
    
    //Log Out
    public function getLogOut(){
        Auth::logout();
        return redirect('/');
    }






    // Admin Dashboard
    public function adminDashboard(){
        
        $data = array(
            'title' => 'Admin Dashboard'
        );
        
        return View::make('admin.pages.admin_dashboard')->with($data);  
    }
    
    
    
    
    //Doctor's Dashboard
    public function doctorDashboard(){
       $data = array(
            'title' => 'Doctor Dashboard'
        );
        
        return View::make('admin.pages.doctor_dashboard')->with($data);  
    }
    
    
    //Patient's Dashboard
    
    public function patientDashboard(){
        $data = array(
            'title' => 'Patient Dashboard'
        );
        
        return View::make('admin.pages.patient_dashboard')->with($data);  
    }

}
