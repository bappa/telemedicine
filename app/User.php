<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

use DB;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['*'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    
     static function createDoctor($doctorInfo){
       $doctor_id = DB::table('users')->insertGetId($doctorInfo);
       return $doctor_id;
    }
    
    static function confiormDoctorEmail($id){
        DB::table('users')
            ->where('id', $id)
            ->update(['mailconfiorm' => 1]);
    }
    
}
