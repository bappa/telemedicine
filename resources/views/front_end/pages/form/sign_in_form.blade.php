@extends('front_end.templates.default')
@section('content')
            <div class="row">
                <div class="col-md-5">
                        <h2>Sign In</h2>
                          <?php echo Form::open(array('class'=>"form-horizontal",'route' => 'addPatient', 'files' => true))?>
                            <div class="form-group">
                                <label for="inputText" class="control-label col-xs-3">User Name:</label>
                                <div class="col-xs-9">
                                    <?php echo Form::text('patient_username', '', $attributes = array('class' => 'form-control','id'=>"inputUsername",'placeholder' => 'Username')); ?>
                                    <span class="text-red"><?php echo $errors->patient->first('patient_username'); ?></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-2">Password:</label>
                                <div class="col-xs-10">
                                    <?php echo Form::password('patient_password', array('class' => 'form-control','id'=>"inputPassword")); ?>
                                    <span class="text-red"><?php echo $errors->patient->first('patient_password'); ?></span>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-xs-offset-2 col-xs-4">
                                     <?php echo Form::submit('Sign In', array('class' => 'btn btn-primary btn-block btn-flat inside_body_submit')) ?>
                                </div>
                            </div>
                          <?php echo Form::close(); ?>
                </div>
                 
            </div>
@stop