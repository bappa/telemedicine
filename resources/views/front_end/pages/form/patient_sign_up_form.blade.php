@extends('front_end.templates.default')
@section('content')

    <div class="row  wow fadeInDown animated animated">
        <div class="col-md-6 col-md-offset-4 ">
            <div class="tlmd_inner_page_heading">
                <h1> Join As a Patient</h1>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
                <hr>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-6 wow fadeInLeft animated">

            <?php echo Form::open(array('class' => "form-horizontal", 'route' => 'addDoctor', 'files' => true))?>

            <div class=" form-group form_section_title "> <h4> Personal Information</h4></div>

            <div class="form-group">
                <label for="inputText" class="control-label col-xs-3"> First Name:</label>
                <div class="col-xs-9">
                    <?php echo Form::text('f_name', '', $attributes = array('class' => 'form-control', 'id' => "f_name", 'placeholder' => ' First Name')); ?>
                    <span class="text-red"><?php echo $errors->first('f_name'); ?></span>
                </div>
            </div>

            <div class="form-group">
                <label for="inputText" class="control-label col-xs-3"> Last Name:</label>
                <div class="col-xs-9">
                    <?php echo Form::text('l_name', '', $attributes = array('class' => 'form-control', 'id' => "l_name", 'placeholder' => ' Last Name')); ?>
                    <span class="text-red"><?php echo $errors->first('l_name'); ?></span>
                </div>
            </div>

            <div class="form-group">
                <label for="patient_gender" class="control-label col-xs-3">Gender:</label>
                <div class="col-xs-9">
                    <div class="col-sm-4">
                        <div class="radio">
                            <label>
                                <?php  echo Form::radio('gender', 'male');?>
                                Male
                                <?php  echo Form::radio('gender', 'female');?>
                                Female
                            </label>
                        </div>
                        <span class="text-red"><?php echo $errors->first('gender'); ?></span>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="bloodgroup" class="control-label col-xs-3">Blood Group:</label>
                <div class="col-xs-9">
                    <?php
                    $bloodgroup = array(
                            'A +' => 'A +',
                            'A -' => 'A -',
                            'B +' => 'B +',
                            'B -' => 'B -',
                            'O +' => 'O +',
                            'O -' => 'O -',
                            'O -' => 'O -',
                            'O -' => 'O -',
                    );
                    echo Form::select('bloodgroup', $bloodgroup,'', $attributes = array('class' => 'form-control')); ?>
                    <span class="text-red"><?php echo $errors->first('bloodgroup'); ?></span>
                </div>
            </div>

            <div class="form-group">
                <label for="nationalId" class="control-label col-xs-3">National Id:</label>
                <div class="col-xs-9">
                    <?php echo Form::number('nidno', '', $attributes = array('class' => 'form-control', 'id' => "inputNID", 'placeholder' => 'National ID No')); ?>
                    <span class="text-red"><?php echo $errors->first('nidno'); ?></span>
                </div>
            </div>
            <div class="form-group">
                <label for="inputBiography" class="control-label col-xs-3"> About: </label>
                <div class="col-xs-9">
                    <?php echo Form::textarea('about', '', $attributes = array('class' => 'form-control', 'id' => "BioGraphy", 'placeholder' => 'BioGraphy')); ?>
                    <span class="text-red"><?php echo $errors->patient->first('abouty'); ?></span>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-3 control-label">Profile Image</div>
                <div class="col-xs-9">
                    <?php echo Form::file('image');?>
                    <span class="text-red"><?php  echo  Session::get('image_validation_error'); ?></span>
                </div>
            </div>

            <div class=" form-group form_section_title "> <h4> Professional Information</h4></div>

            <div class="form-group">
                <label for="medicalReg" class="control-label col-xs-3">Medical Reg No:</label>
                <div class="col-xs-9">
                    <?php echo Form::number('medical_reg_no', '', $attributes = array('class' => 'form-control', 'id' => "medical_reg_no", 'placeholder' => 'Medical Reg No')); ?>
                    <span class="text-red"><?php echo $errors->first('medical_reg_no'); ?></span>
                </div>
            </div>
            <div class="form-group">
                <label for="inputText" class="control-label col-xs-3"> Present Job:</label>
                <div class="col-xs-9">
                    <?php echo Form::text('p_job', '', $attributes = array('class' => 'form-control', 'id' => "l_name", 'placeholder' => 'Present Job')); ?>
                    <span class="text-red"><?php echo $errors->first('p_job'); ?></span>
                </div>
            </div>


            <div class="form-group">
                <label for="medicalSpeciality" class="control-label col-xs-3">Speciality:</label>
                <div class="col-xs-9">
                    <?php
                    $medical_speciality = array(
                            'General Medicine' => 'General Medicine',
                            'General Surgery'  => 'General Surgery',
                            'Gynecology'       => 'Gynecology',
                            'Pediatrics'       => 'Pediatrics',
                            'Anesthesiology'   => 'Anesthesiology',
                            'Psychiatry'       => 'Psychiatry'
                    );
                    echo Form::select('medical_speciality', $medical_speciality,'', $attributes = array('class' => 'form-control')); ?>
                    <span class="text-red"><?php echo $errors->first('medical_speciality'); ?></span>
                </div>
            </div>


            <div class=" form-group form_section_title "> <h4> Contact Information</h4></div>

            <div class="form-group">
                <label for="inputPhone" class="control-label col-xs-3">Mobile:</label>
                <div class="col-xs-9">
                    <?php echo Form::number('patient_mobile', '', $attributes = array('class' => 'form-control', 'id' => "inputMobile", 'placeholder' => 'Mobile No')); ?>
                    <span class="text-red"><?php echo $errors->patient->first('patient_mobile'); ?></span>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail" class="control-label col-xs-3">Email:</label>
                <div class="col-xs-9">
                    <?php echo Form::text('patient_email', '', $attributes = array('class' => 'form-control', 'id' => "inputEmail", 'placeholder' => 'Email ID')); ?>
                    <span class="text-red"><?php echo $errors->patient->first('patient_email'); ?></span>
                </div>
            </div>


            <div class=" form-group form_section_title "> <h4> Account Information</h4></div>

            <div class="form-group">
                <label for="inputText" class="control-label col-xs-3">User Name:</label>
                <div class="col-xs-9">
                    <?php echo Form::text('patient_username', '', $attributes = array('class' => 'form-control', 'id' => "inputUsername", 'placeholder' => 'Username')); ?>
                    <span class="text-red"><?php echo $errors->patient->first('patient_username'); ?></span>
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword" class="control-label col-xs-3">Password:</label>
                <div class="col-xs-9">
                    <?php echo Form::password('patient_password', array('class' => 'form-control', 'id' => "inputPassword")); ?>
                    <span class="text-red"><?php echo $errors->patient->first('patient_password'); ?></span>
                </div>
            </div>


            <div class="form-group">
                <div class="col-xs-offset-3 col-xs-9">
                    <?php echo Form::submit('Sign Up', array('class' => 'btn btn-primary btn-block btn-flat inside_body_submit')) ?>
                </div>
            </div>
            <?php echo Form::close(); ?>
        </div>
        <div class="col-md-6 wow fadeInRightBig animated">
            <div class="tlmd_form_page_image">
                <img src="{{URL::to('public/assets/frontend/img/doctor.jpg')}}"/>
            </div>
        </div>

    </div>
@stop